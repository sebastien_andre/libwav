#!/usr/bin/env python
# coding: utf-8

# ceci est le module libwav
import wave
import matplotlib.pyplot as plt
import numpy as np

def convertit_en_entiers_signés(liste_d_octets_LE_en_comp_à_2):
    """ Utilisée par lireWav. """
    liste_resultat = []
    indicepair = 0 
    
    while indicepair < len(liste_d_octets_LE_en_comp_à_2):
        octetpair = liste_d_octets_LE_en_comp_à_2[indicepair]
        octetimpair = liste_d_octets_LE_en_comp_à_2[indicepair + 1]
        valabs = octetpair + 256 * octetimpair
        if valabs >= 0x7FFF: # négatif
            liste_resultat.append( valabs - 0x10000 ) 
        else:
            liste_resultat.append(valabs)
        indicepair += 2 
    return liste_resultat

def lireWav(nomFichier):
    """ Lit un fichier wav mono et renvoie un dictionnaire.
    """
    with wave.open(nomFichier, mode="rb") as wr:
        return lireWavHandle(wr)
    
def lireWavHandle(h):
    """ Auxiliaire de lireWav pour tests.
    """
    h.setpos(0) # seek(0) n'existe plus
    ncha = h.getnchannels() #    Renvoie le nombre de canaux audio (1 pour mono, 2 pour stéréo).
    assert ncha == 1 ,f"Le fichier {h} a {ncha} composantes: il n'est pas mono."
    noct = h.getsampwidth() #    Renvoie la largeur de l'échantillon en octets.
    frsa = h.getframerate() #    Renvoie la fréquence d'échantillonnage.
    nfra = h.getnframes()   #    Renvoie le nombre de trames audio.
    comp = h.getcomptype()  #    Renvoie le type de compression ('NONE' est le seul type géré).
    cpna = h.getcompname()  #    Version compréhensible de getcomptype(). Généralement, 'not compressed' équivaut à 'NONE'.
    allf = h.readframes(nfra) #    Lit et renvoie au plus n trames audio, sous forme d'objet bytes.
    #h.close()
    return {
        "ncha": ncha,
        "noct": noct,
        "frsa": frsa,
        "nfra": nfra,
        "comp": comp,
        "cpna": cpna,
        "vals": convertit_en_entiers_signés(allf)       
    }

# Remarques : 
#     
# -    nchannels=1 : mono 
# -    sampwidth=2 : 2 octets par échantillons soit 16 bits
# -    framerate=48000 : 48000 échantillons / s
# -    nframes=47860 : on a un peu moins d'une seconde ici
# -    comptype='NONE' 
# -    compname='not compressed' 

def convertit_en_octets_en_comp_à_2(liste_d_entiers_signés) -> bytearray:
    liste_resultat = bytearray()
    for n in liste_d_entiers_signés:
        
        if n<0: 
            valabs = 0x10000 + n
        else:
            valabs = n 
        octetpair = valabs% 256
        octetimpair  = valabs // 256
        liste_resultat.append(octetpair)
        liste_resultat.append(octetimpair)
    return liste_resultat

def littleEndianBytearrayToIntList(a):
            return [int(x) for x in a]

# mettre dans les tests
# assert convertit_en_octets_en_comp_à_2 (convertit_en_entiers_signés ( debut ) ) == debut

def ecrireWav(vals, nomFichier, frsa = 48000):
    """ Écrit un fichier .wav mono à partir de la liste vals.
    """
    with wave.open(nomFichier, mode="wb") as ww:
        ecrireWavHandle(vals, ww, frsa)

def ecrireWavHandle(vals, h, frsa = 48000):
    """ Auxiliaire de ecrireWav pour tests.
    """
    h.setnchannels(1) # mono obligatoire
    h.setsampwidth(2) # 2 octets/éch
    h.setframerate(frsa)
    h.setnframes(len(vals))
    #h.seek(0) 
    h.writeframes(convertit_en_octets_en_comp_à_2(vals))
    #h.close()
    
def graph(vals, *autresvals):
    fig, ax = plt.subplots()

    nmax = max([len(vals)] + [len(v) for v in autresvals])
    t = range(0, nmax)
    i = 1
    for v in [vals] + list(autresvals) :
        padded = v + [np.nan] * (nmax-len(v)) if len(v) < nmax else v
        ax.plot(t, padded, alpha = .5, label=f"{i}")
        i += 1
    ax.legend()
    plt.show()
