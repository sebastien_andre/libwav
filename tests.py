import random
import unittest
from io import BytesIO

import libwav

class libwavTests(unittest.TestCase):
    
    def setUp(self):
        ...
        
    def tearDown(self):
        ...
             
    def test_littleEndianBytearrayToIntList(self):
        b = bytearray(b'\xff\xb2\xff\xc5')
        self.assertEqual(libwav.littleEndianBytearrayToIntList(b),[255, 178, 255, 197])
    
    def test_ecrireEtLire(self):
        zeroPartout = [0] * 48000
        outfile = "tel.wav" # BytesIO()
        libwav.ecrireWav(zeroPartout, outfile)
        content = libwav.lireWav(outfile)["vals"]
        self.assertEqual(zeroPartout, content)
        
    def test_ent_sign_comp_à_2(self):
        listeEntRandom = []
        for i in range(24000):
            listeEntRandom.append(random.randint(-32768,32767))
        liste_ent_comp_à_2 = libwav.convertit_en_octets_en_comp_à_2(listeEntRandom )
        liste_ent_reconvertis = libwav.convertit_en_entiers_signés(liste_ent_comp_à_2)
        self.assertEqual(liste_ent_reconvertis, listeEntRandom) 
         
if __name__ == '__main__':
    unittest.main()