# libwav ∿

Une bibliothèque très simple pour manipuler des fichier sonores au format `.wav`.

## Utilisation

Le propos de cette bibliothèque est de modifier des sons en agissant sur la liste des valeurs
constituant le signal sonore.

Le format `.wav` s'y prête bien car le son n'est pas compressé. Le signal est enregistré très simplement par la liste des valeurs représenant l'_onde_ sonore (_wave_) ∿.

Pour cela le module `libwav` propose les fonctions suivantes.

### Importation des données sonores 
`lireWav(nomFichier)`

lit le fichier .wav donné en paramètre. 

Renvoie un dictionnaire donnant les caractéristiques du son enregistré :

```python
{
"ncha": # le nombre de canaux (1: mono, 2: stéréo)
"noct": # le nombre d'octets utilisés pour coder un échantillon (habituellement 2)
"frsa": # la fréquence d'échantillonnage (de l'ordre de 48000 Hz souvent)
"nfra": # le nombre d'échantillons 
"comp": # le type de compression ('NONE' est le seul type géré)
"cpna": # Généralement, 'not compressed' équivaut à 'NONE'.
"vals": # le signal lui-même sous forme de liste de valeurs
}
```

Pour aller à l'essentiel, on peut ignorer les premières composantes et ne travailler que sur la dernière: les échantillons sonores.

Exemple:

```python
import libwav
w = libwav.lireWav("SFO-1s.wav")
son1 = w["vals"]
print(son1)

[33, 54, 88, 124, ...]
```


### Visualisation du signal sonore 

`graph(liste_1, liste_2, ...)`

permet de visualiser une ou plusieurs listes de valeurs représentant des signaux sonores.

Exemple:

```python
libwav.graph(son1)
```
![vals](output-vals.png) 


### Enregistrement d'un signal sonore

`ecrireWav(vals, nomFichier, frsa = 48000)` 

enregistre le signal donné sous forme de liste `vals`dans le fichier nommé `nomFichier` avec une fréquence d'échantillonnage par défaut de 48000 Hz.

Le son est mono uniquement.

> Exemple:

> ```
> > son2 = son1[0:24000]
> > libwav.ecrireWav("SFO-coupé.wav", son2)
> > libwav.graph(son1, son2)
> ```

![v+coupé](output-vals-coupe.png)

Le graphe ci-desus présente le son initial et, superposé à celui-ci, le son obtenu en coupant le premier à 0,5s.